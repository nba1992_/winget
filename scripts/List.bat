echo off
echo # # # # # # # # # # # # #
echo -----Winget List -----
echo # # # # # # # # # # # # #

set ARGUMENTS=0

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

set GLOBAL_ARGS=--verbose-logs --accept-source-agreements
echo winget list %GLOBAL_ARGS%
winget list %GLOBAL_ARGS%