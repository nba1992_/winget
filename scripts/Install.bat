echo off
echo # # # # # # # # # # # # #
echo -----Winget Install -----
echo # # # # # # # # # # # # #

set ARGUMENTS=1

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)
set APP_NAME=%1
set APP_LOCATION=D:\programs\%APP_NAME%
set GLOBAL_ARGS=--verbose-logs --accept-source-agreements --accept-package-agreements
set CMD_ARGS=--location %APP_LOCATION%
echo winget install %APP_NAME% %GLOBAL_ARGS% %CMD_ARGS%
winget install %APP_NAME% %GLOBAL_ARGS% %CMD_ARGS%