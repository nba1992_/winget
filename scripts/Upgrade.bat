echo off
echo # # # # # # # # # # # # #
echo -----Winget Upgrade -----
echo # # # # # # # # # # # # #

set ARGUMENTS=0

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

set GLOBAL_ARGS=--verbose-logs --accept-source-agreements --accept-package-agreements
set CMD_ARGS=--all
echo winget upgrade %GLOBAL_ARGS% %CMD_ARGS%
winget upgrade %GLOBAL_ARGS% %CMD_ARGS%