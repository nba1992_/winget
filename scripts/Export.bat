echo off
echo # # # # # # # # # # # # #
echo -----Winget Export -----
echo # # # # # # # # # # # # #

set ARGUMENTS=0

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

set LOG_FILENAME=%~n0.json
set GLOBAL_ARGS=--verbose-logs --accept-source-agreements --include-versions
set CMD_ARGS=--output %LOG_FILENAME%
echo winget export %GLOBAL_ARGS% %CMD_ARGS%
winget export %GLOBAL_ARGS% %CMD_ARGS%