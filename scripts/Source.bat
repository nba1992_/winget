echo off
echo # # # # # # # # # # # # #
echo -----Winget Sources -----
echo # # # # # # # # # # # # #

set ARGUMENTS=0

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)

set GLOBAL_ARGS=--verbose-logs
echo winget source list %GLOBAL_ARGS%
winget source list %GLOBAL_ARGS%
echo winget source update %GLOBAL_ARGS%
winget source update %GLOBAL_ARGS%
echo winget source export %GLOBAL_ARGS%
winget source export %GLOBAL_ARGS%