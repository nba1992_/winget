echo off
echo # # # # # # # # # # # # #
echo -----Winget Search -----
echo # # # # # # # # # # # # #

set ARGUMENTS=1

set argC=0
for %%x in (%*) do	(
	echo %%x
	Set /A argC+=1
)
echo arguments:%argC%

if  %argC% NEQ %ARGUMENTS%  (
    echo Parameters missing: #%argC% should be %ARGUMENTS%
    exit 1
)
set APP_NAME=%1
set GLOBAL_ARGS=--verbose-logs --accept-source-agreements
echo winget search %APP_NAME% %GLOBAL_ARGS%
winget search %APP_NAME% %GLOBAL_ARGS%