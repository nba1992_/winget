# Winget #

This Winget repository is like DIY to first use this software.

### What is Winget? ###

* [Winget](https://winget.run/)

### NOTES ###

* Unfortunately, even with non-default folders(D:\\...), you have to execute any of this script with elevated permissions.

### How do I set up Winget? ###

* It cames bundled in most recent windows versions.
* Otherwise you can download from [Releases Page](https://github.com/microsoft/winget-cli/releases).

### How do I get install packages? ###

* Configure apps.json with the desired software.
* Execute Import.bat in order to install desired software.

### How do I update packages? ###

* Execute Upgrade.bat in order to update all packages.

### How do I upgrade Winget? ###

* Don't know yet.

### Utilities script ###

* Export.bat.
* Features.bat.
* List.bat.
* Search.bat.
* Show.bat.
* Sources.bat.

### License ###

FREE!!!

### Review/Changes/Maintenance ###

* Feel free to open PR